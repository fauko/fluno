import 'package:get/get.dart';

import '../modules/chose_payment/bindings/chose_payment_binding.dart';
import '../modules/chose_payment/views/chose_payment_view.dart';
import '../modules/dasboard/bindings/dasboard_binding.dart';
import '../modules/dasboard/views/dasboard_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/nfc_payment/bindings/nfc_payment_binding.dart';
import '../modules/nfc_payment/views/nfc_payment_view.dart';
import '../modules/qris_payment/bindings/qris_payment_binding.dart';
import '../modules/qris_payment/views/qris_payment_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.DASBOARD,
      page: () => const DasboardView(),
      binding: DasboardBinding(),
    ),
    GetPage(
      name: _Paths.CHOSE_PAYMENT,
      page: () => const ChosePaymentView(),
      binding: ChosePaymentBinding(),
    ),
    GetPage(
      name: _Paths.QRIS_PAYMENT,
      page: () => const QrisPaymentView(),
      binding: QrisPaymentBinding(),
    ),
    GetPage(
      name: _Paths.NFC_PAYMENT,
      page: () => const NfcPaymentView(),
      binding: NfcPaymentBinding(),
    ),
  ];
}
