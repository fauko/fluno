part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const DASBOARD = _Paths.DASBOARD;
  static const CHOSE_PAYMENT = _Paths.CHOSE_PAYMENT;
  static const QRIS_PAYMENT = _Paths.QRIS_PAYMENT;
  static const NFC_PAYMENT = _Paths.NFC_PAYMENT;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const DASBOARD = '/dasboard';
  static const CHOSE_PAYMENT = '/chose-payment';
  static const QRIS_PAYMENT = '/qris-payment';
  static const NFC_PAYMENT = '/nfc-payment';
}
