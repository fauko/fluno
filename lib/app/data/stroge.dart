import 'package:get_storage/get_storage.dart';

class UserPreferences {
  static final _storage = GetStorage(
    
  );
  static const _keyLoggedIn = 'isLoggedIn';
  static const _keyToken = 'token';

  static bool get isLoggedIn => _storage.read(_keyLoggedIn) ?? false;
  static String get token => _storage.read(_keyToken) ?? '';

  static Future<void> setLoggedIn(bool value) async {
    await _storage.write(_keyLoggedIn, value);
  }

  static Future<void> setToken(String token) async {
    await _storage.write(_keyToken, token);
  }

  static Future<void> clear() async {
    await _storage.erase();
  }
}
