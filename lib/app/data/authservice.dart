import 'dart:convert';
import 'package:fluno/app/data/UserModel.dart';
import 'package:fluno/app/data/helper.dart';
import 'package:fluno/app/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';

class AuthService {
  static Future<String?> login(String email, String password) async {
    final response = await http.post(
      Uri.parse('${Helper.apiUrl}/login.php'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({'email': email, 'password': password}),
    );
    if (response.statusCode == 200) {
      final responseData = jsonDecode(response.body);
      Logger().i(responseData);
      UserModel(
        email: responseData[2],
        id: int.parse(responseData[0]),
        nama: responseData[1],
      );
      final token = responseData[0];
      return token;
    } else {
      return null;
    }
  }
}
