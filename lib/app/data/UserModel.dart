import 'dart:convert';

List<UserModel> userModelFromMap(String str) =>
    List<UserModel>.from(json.decode(str).map((x) => UserModel.fromMap(x)));

String userModelToMap(List<UserModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class UserModel {
  UserModel({
    required this.id,
    required this.nama,
    required this.email,
  });

  int id;
  String nama;
  String email;

  factory UserModel.fromMap(Map<String, dynamic> json) => UserModel(
        id: json["id"],
        nama: json["nama"],
        email: json["email"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "nama": nama,
        "email": email,
      };
}


// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

// import 'dart:convert';

// UserModel userModelFromJson(String str) => UserModel.fromJson(jsonDecode(str));

// String userModelToJson(UserModel data) => jsonEncode(data.toJson());

// class UserModel {
//   UserModel({
//     required this.id,
//     required this.nama,
//     required this.email,
//     required this.password,
//   });

//   int id;
//   String nama;
//   String email;
//   String password;

  // factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
  //       id: json["id"],
  //       nama: json["nama"],
  //       email: json["email"],
  //       password: json["password"],
  //     );

  // Map<String, dynamic> toJson() => {
  //       "id": id,
  //       "nama": nama,
  //       "email": email,
  //       "password": password,
  //     };
//}
