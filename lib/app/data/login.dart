import 'package:fluno/app/data/authservice.dart';
import 'package:fluno/app/data/stroge.dart';
import 'package:fluno/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class LoginController extends GetxController {
  late TextEditingController emailController;
  late TextEditingController passwordController;
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final isLoading = false.obs;
  final token = ''.obs;
  Future<void> login() async {
    try {
      isLoading.value = true;
      final email = emailController.text;
      final password = passwordController.text;
      token.value = (await AuthService.login(email, password))!;
      if (token.value != '') {
        await UserPreferences.setLoggedIn(true);
        await UserPreferences.setToken(token.value);
        Logger().i(token);
        Get.offAllNamed(Routes.DASBOARD);
        // Navigate to home page or other page
      } else {
        // Show error message
      }
    } finally {
      isLoading.value = false;
    }
  }

  @override
  void onInit() {
    token.value;
    emailController = TextEditingController();
    passwordController = TextEditingController();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    emailController.dispose();
    passwordController.dispose();
    super.onClose();
  }
}
