import 'package:fluno/app/data/Protocol.dart';
import 'package:fluno/app/data/UserModel.dart';
import 'package:fluno/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeController extends GetxController {
  // final authService = AuthService();
  final RxBool isLoading = false.obs;
  final RxBool rememberMe = false.obs;
  late TextEditingController emailController;
  late TextEditingController passwordController;
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();

  // Future<void> login() async {
  //   try {
  //     isLoading.value = true;
  //     UserModel user = await authService.login(
  //         emailController.text, passwordController.text);
  //     Logger().i(user.email);
  //     // Jika user sudah berhasil login, kita simpan data user tersebut di Shared Preferences
  //     if (user.email != '') {
  //       // Contoh menyimpan data user menggunakan shared_preferences
  //       // Anda juga bisa menggunakan database atau state management lainnya
  //       SharedPreferences prefs = await SharedPreferences.getInstance();
  //       prefs.setInt('id', int.parse(user.id.toString()));
  //       prefs.setString('nama', user.nama.toString());
  //       prefs.setString('email', user.email.toString());
  //       prefs.setString('password', user.password.toString());
  //     Get.offAllNamed(Routes.DASBOARD, arguments: user);
  //     isLoading.value = false;
  //     }
  //   } catch (e) {
  //     isLoading.value = false;
  //     // Logger().i(e);
  //     Get.snackbar('Error', 'Failed to login');
  //   }
  // }

  @override
  void onInit() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    emailController.dispose();
    passwordController.dispose();
    super.onClose();
  }

}

//   final count = 0.obs;

//   void increment() => count.value++;
// }
