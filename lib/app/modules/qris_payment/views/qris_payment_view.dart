import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/qris_payment_controller.dart';

class QrisPaymentView extends GetView<QrisPaymentController> {
  const QrisPaymentView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('QrisPaymentView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'QrisPaymentView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
