import 'package:get/get.dart';

import '../controllers/nfc_payment_controller.dart';

class NfcPaymentBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<NfcPaymentController>(
      () => NfcPaymentController(),
    );
  }
}
