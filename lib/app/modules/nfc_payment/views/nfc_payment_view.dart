import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/nfc_payment_controller.dart';

class NfcPaymentView extends GetView<NfcPaymentController> {
  const NfcPaymentView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('NfcPaymentView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'NfcPaymentView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
