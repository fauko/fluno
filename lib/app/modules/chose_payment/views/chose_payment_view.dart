import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/chose_payment_controller.dart';

class ChosePaymentView extends GetView<ChosePaymentController> {
  const ChosePaymentView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ChosePaymentView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'ChosePaymentView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
