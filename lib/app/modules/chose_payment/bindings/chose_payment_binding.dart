import 'package:get/get.dart';

import '../controllers/chose_payment_controller.dart';

class ChosePaymentBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ChosePaymentController>(
      () => ChosePaymentController(),
    );
  }
}
