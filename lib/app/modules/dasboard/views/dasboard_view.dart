import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../controllers/dasboard_controller.dart';

class DasboardView extends GetView<DasboardController> {
  const DasboardView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var data = Get.arguments;
    //  Logger().i(data[2]);
    return Scaffold(
      appBar: AppBar(
        title: const Text('DasboardView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'DasboardView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
