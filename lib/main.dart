import 'dart:io';

import 'package:fluno/app/data/login.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:logger/logger.dart';

import 'app/data/authservice.dart';
import 'app/routes/app_pages.dart';

Future<void> main() async {
  //final server = await ServerSocket.bind(InternetAddress.anyIPv4, 80);
  // Socket sock = await Socket.connect('192.168.2.148', 80);
  // Logger().i(sock.done);

  Get.put(LoginController(), permanent: true);
  var cons = Get.put(LoginController());
  await GetStorage.init();

  runApp(
      // StreamBuilder<AuthService?>(
      //     stream: cons.token,
      //   builder: (((context, snapshot) {
      // if (snapshot.connectionState == ConnectionState.waiting) {
      //   return Obx(() => const Center(child: CircularProgressIndicator()));
      // }
      GetMaterialApp(
    debugShowCheckedModeBanner: false,
    debugShowMaterialGrid: false,
    initialRoute: cons.token.isNotEmpty ? Routes.DASBOARD : Routes.HOME,
    getPages: AppPages.routes,
  )
      //})))
      );

  // sock.listen((List<int> data) {
  //   String s = String.fromCharCodes(data).trim();
  //   Logger().i(s);
  // });
}
